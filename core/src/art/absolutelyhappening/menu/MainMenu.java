package art.absolutelyhappening.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import art.absolutelyhappening.game.ClientGameScreen;
import art.absolutelyhappening.game.GameScreen;
import art.absolutelyhappening.happening.happening;

/**
 * Created by Dluuugi on 24/11/2017.
 */

public class MainMenu implements Screen {

    private happening game;
    private Stage stage;

    public MainMenu(final happening game) {
        this.game = game;
        stage = new Stage(new StretchViewport(1000,500));
        Label title = new Label("Main menu", happening.gameSkin);
        title.setAlignment(Align.center);
        title.setY(Gdx.graphics.getHeight()*2/3);
        title.setWidth(Gdx.graphics.getWidth());
        title.setFontScale(5);
        stage.addActor(title);

        Texture hostButtonTexture = new Texture("ui/button_host_game.png");
        Texture hostButtonTexture_p = new Texture("ui/button_host_game_p.png");
        ImageButton hostGameButton = new ImageButton(new Image(hostButtonTexture).getDrawable(), new Image(hostButtonTexture_p).getDrawable());
        hostGameButton.setTransform(true);
        //hostGameButton.setSize(300,200);
        hostGameButton.setPosition(400,100);
        hostGameButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new HostMenu(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(hostGameButton);

        Texture joinButtonTexture = new Texture("ui/button_join_game.png");
        Texture joinButtonTexture_p = new Texture("ui/button_join_game_p.png");
        ImageButton joinGameButton = new ImageButton(new Image(joinButtonTexture).getDrawable(),new Image(joinButtonTexture_p).getDrawable());
        joinGameButton.setTransform(true);
        //joinGameButton.setSize(300,200);
        joinGameButton.setPosition(400,200);
        joinGameButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new ClientGameScreen(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(joinGameButton);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
