package art.absolutelyhappening.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.World;
import com.codeandweb.physicseditor.PhysicsShapeCache;

import java.util.ArrayList;

import art.absolutelyhappening.happening.BodySprite;
import art.absolutelyhappening.happening.CollisionManager;
import art.absolutelyhappening.happening.Ship;
import art.absolutelyhappening.happening.happening;
import art.absolutelyhappening.menu.FailScreen;
import art.absolutelyhappening.menu.WinScreen;

/**
 * Created by Dluuugi on 24/11/2017.
 */

public class GameScreen implements Screen {
    private happening game;


    World world;
    Ship ship;
    ArrayList<BodySprite> bodies;
    PhysicsShapeCache physicsBodies;
    SpriteBatch batch;
    Texture img;
    Texture terrainTexture;
    Texture platformTexture;
    Texture background;
    Sprite backgroundSprite;
    CollisionManager collisionManager;

    public GameScreen(happening game) {
        this.game = game;
        Box2D.init();
		bodies = new ArrayList<BodySprite>();
		batch = new SpriteBatch();
		img = new Texture("spaceship1.png");
		terrainTexture = new Texture("terrain.png");
		platformTexture = new Texture("platform.png");
		world = new World(new Vector2(0,-1f),false);
		collisionManager = new CollisionManager(this);
		world.setContactListener(collisionManager);
		physicsBodies = new PhysicsShapeCache("physics.xml");

		background = new Texture("background.jpg");
		backgroundSprite = new Sprite(background);
		backgroundSprite.setSize(30000,30000);
		backgroundSprite.setPosition(-5000,-5000);

        bodies.add(new BodySprite(terrainTexture, new Vector2(0, 0),
				physicsBodies.createBody("terrain", world, 1/BodySprite.PX_TO_METER, 1/BodySprite.PX_TO_METER)));

        bodies.add(new BodySprite(platformTexture, new Vector2(25, 10),
                physicsBodies.createBody("platform", world, 1/BodySprite.PX_TO_METER, 1/BodySprite.PX_TO_METER)));

		ship = new Ship(img,new Vector2(15,10),
				physicsBodies.createBody("spaceship1",world,1/BodySprite.PX_TO_METER,1/BodySprite.PX_TO_METER));
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void render(float delta) {
        ship.update(game.netw.getClientThruster(),Gdx.input.isTouched()? 1.0f : 0.0f);
        if (collisionManager.hasLanded()) {
            game.netw.sendPosition(
                    ship.getBody().getPosition().x,
                    ship.getBody().getPosition().y,
                    ship.getBody().getAngle(),
                    Gdx.input.isTouched()? 1.0f : 0.0f,
                    1f);
            Gdx.app.log("LANDING","landed");
            game.setScreen(new WinScreen(this.game));
        }
        if (collisionManager.hasCrashed()) {
            game.netw.sendPosition(
                    ship.getBody().getPosition().x,
                    ship.getBody().getPosition().y,
                    ship.getBody().getAngle(),
                    Gdx.input.isTouched()? 1.0f : 0.0f,
                    2f);
            game.setScreen(new FailScreen(this.game));
            Gdx.app.log("LANDING","crashed");
        }
        world.step(delta, 5, 5);
        batch.setProjectionMatrix(ship.getCamera().combined);

        game.netw.sendPosition(
                ship.getBody().getPosition().x,
                ship.getBody().getPosition().y,
                ship.getBody().getAngle(),
                Gdx.input.isTouched()? 1.0f : 0.0f,
                0f);


        Gdx.gl.glClearColor(game.netw.getClientThruster(), 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        backgroundSprite.draw(batch);
        for (BodySprite body : bodies) {
            body.getSprite().draw(batch);
        }
        ship.getSprite().draw(batch);
        batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
