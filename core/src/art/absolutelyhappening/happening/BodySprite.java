package art.absolutelyhappening.happening;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by ja on 24.11.17.
 *
 * box2d body integrated with libgdx sprite
 */

public class BodySprite {

    public static final float PX_TO_METER = 50;

    private final Sprite sprite;
    protected Body body;

    public BodySprite(Texture texture, Vector2 position, Body body){
        sprite = new Sprite(texture);
        sprite.setOriginCenter();
        this.body = body;
        body.setTransform(position,body.getAngle());
    }

    public Sprite getSprite(){
        sprite.setPosition(body.getPosition().x*PX_TO_METER-sprite.getWidth()/2,body.getPosition().y*PX_TO_METER-sprite.getHeight()/2);
        sprite.setRotation((float) Math.toDegrees(body.getAngle()));
        return sprite;
    }

    public Body getBody(){
        return body;
    }
}
