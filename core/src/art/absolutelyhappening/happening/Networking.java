package art.absolutelyhappening.happening;

import java.net.InetAddress;

import art.absolutelyhappening.menu.HostMenu;

/**
 * Created by nieMagda on 24.11.2017.
 */

public interface Networking {

    public void setClientThruster(float f);
    public float getClientThruster();
    public void setHostThruster(float f);
    public float getHostThruster();
    public void setX(float f);
    public float getX();
    public void setY(float f);
    public float getY();
    public void setR(float f);
    public float getR();
    public void setGameState(float f);
    public float getGameState();

    public String checkIP();
    public boolean isNewPosition();
    public void startUDP();

    public void startListeningForThruster(HostMenu hm);
    public void startListeningForPosition();

    public void sendPosition(float x,float y,float r,float isOn,float state);
    public void sendThrusterStatus(float f, InetAddress IPAddress );
    public int getClientPort();


}
