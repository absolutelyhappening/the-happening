package art.absolutelyhappening.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import art.absolutelyhappening.game.GameScreen;
import art.absolutelyhappening.happening.happening;

/**
 * Created by Dluuugi on 24/11/2017.
 */

public class FailScreen implements Screen {
    private happening game;
    private Stage stage;

    public FailScreen(happening game) {
        this.game = game;
        stage = new Stage(new ScreenViewport());

        Label title = new Label("failed", happening.gameSkin);
        title.setAlignment(Align.center);
        title.setY(Gdx.graphics.getHeight()*2/3);
        title.setWidth(Gdx.graphics.getWidth());
        title.setFontScale(5);
        stage.addActor(title);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(game.netw.getClientThruster(), 1, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

        if(Gdx.input.isTouched()){
            game.setScreen(new MainMenu(this.game));
        }
        if(game.netw.getClientPort() != 0){

            game.netw.sendPosition(
                    0,0,0,
                    Gdx.input.isTouched()? 1.0f : 0.0f,
                    2f);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
