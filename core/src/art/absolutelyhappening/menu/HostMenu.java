package art.absolutelyhappening.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.net.InetAddress;
import java.net.UnknownHostException;

import art.absolutelyhappening.game.GameScreen;
import art.absolutelyhappening.happening.happening;

/**
 * Created by Dluuugi on 24/11/2017.
 */

public class HostMenu implements Screen {
    private happening game;
    private Stage stage;
    private boolean gonext=false;

    public HostMenu(happening game) {
        this.game = game;
        stage = new Stage(new ScreenViewport());

        game.netw.checkIP();
        game.netw.startUDP();
        game.netw.startListeningForThruster(this);



        Label title = new Label("Host menu", happening.gameSkin);
        title.setAlignment(Align.center);
        title.setY(Gdx.graphics.getHeight()*2/3);
        title.setWidth(Gdx.graphics.getWidth());
        title.setFontScale(5);
        stage.addActor(title);

        Gdx.app.log("MAINE", game.netw.checkIP());

        Label ipLabel = new Label(game.netw.checkIP(), happening.gameSkin);
        ipLabel.setAlignment(Align.center);
        ipLabel.setY(Gdx.graphics.getHeight()*1/2);
        ipLabel.setWidth(Gdx.graphics.getWidth());
        ipLabel.setFontScale(5);
        stage.addActor(ipLabel);

    }

    public void gotoGame(){

        gonext=true;
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(game.netw.getClientThruster(), 1, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        if(gonext) {
            game.setScreen(new GameScreen(this.game));
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
