package art.absolutelyhappening.happening;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by Dluuugi on 25/11/2017.
 */

public class Platform extends BodySprite {
    public Platform(Texture texture, Vector2 position, Body body) {
        super(texture, position, body);
    }

    public boolean hasLanded(Ship ship) {
        Vector2 shipPosition = ship.body.getPosition();
        float shipAngle = (float)(ship.body.getAngle() % (2 * Math.PI));
        Vector2 platformPosition = body.getPosition();

        if (shipAngle == 0) {
            return true;
        } else {
            return  false;
        }
    }
}
