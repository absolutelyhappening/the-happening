package art.absolutelyhappening.happening;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.World;
import com.codeandweb.physicseditor.PhysicsShapeCache;

import java.util.ArrayList;
import art.absolutelyhappening.menu.MainMenu;


public class happening extends Game {

	static public Skin gameSkin;
    public Networking netw;

    public happening(Networking networking) {
        netw = networking;

    }
	@Override
	public void create() {
		gameSkin = new Skin(Gdx.files.internal("skin/neon-ui.json"));
		this.setScreen(new MainMenu(this));
	}
	@Override
	public void render() {
		super.render();

	}
	
	@Override
	public void dispose () {
	}
}
