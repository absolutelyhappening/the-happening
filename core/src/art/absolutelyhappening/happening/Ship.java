package art.absolutelyhappening.happening;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by ja on 24.11.17.
 */

public class Ship extends BodySprite {

    private static final float X_ABS = 3000;

    private static final Vector2 THRUSTER_FORCE = new Vector2(0,150);
    private static final Vector2 THRUSTER_OFFSET_LEFT = new Vector2(-1f,-0.5f);
    private static final Vector2 THRUSTER_OFFSET_RIGHT = new Vector2(1f,-0.5f);
    private float thrustLeft = 0, thrustRight = 0;

    private OrthographicCamera camera;

    public Ship ( Texture texture, Vector2 position, Body body){
        super(texture,position,body);
        this.body.setUserData(this);
        camera = new OrthographicCamera(
            X_ABS,
            X_ABS*Gdx.graphics.getHeight()/Gdx.graphics.getWidth()
        );

    }

    public void update(float thrustLeft, float thrustRight){
        camera.update();
        this.thrustLeft=thrustLeft;
        this.thrustRight=thrustRight;


    applyForceLocal(new Vector2(THRUSTER_FORCE).scl(thrustLeft), THRUSTER_OFFSET_LEFT);
    applyForceLocal(new Vector2(THRUSTER_FORCE).scl(thrustRight), THRUSTER_OFFSET_RIGHT);
        camera.position.set(body.getPosition().scl(BodySprite.PX_TO_METER),0);
    }

    public void applyForceLocal(Vector2 localForce, Vector2 localPoint){
        body.applyForce(body.getWorldVector(localForce),body.getWorldPoint(localPoint),true);
    }

    public Camera getCamera(){
        return camera;
    }

    public void setState(Vector2 position, float angle){
        body.setTransform(position,angle);
    }
}
