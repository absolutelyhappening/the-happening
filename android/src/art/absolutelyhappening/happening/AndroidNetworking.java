package art.absolutelyhappening.happening;

/**
 * Created by Vajdera on 24.11.2017.
 */

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.io.IOException;
import java.net.SocketException;
import java.util.function.Consumer;

import art.absolutelyhappening.menu.HostMenu;


public class AndroidNetworking implements Networking{

    DatagramSocket senderSocket;
    InetAddress clientAddres;
    int clientPort = 0;

    public float gameState;
    public float clientThruster;
    public float x;
    public float y;
    public float r;
    public float hostThruster;
    boolean newPosition=false;

    public int getClientPort(){
        return clientPort;
    }

    public void setGameState(float f){
        gameState = f;
    }
    public float getGameState(){
        return gameState;
    }

    public void setClientThruster(float f){
        clientThruster = f;
    }
    public float getClientThruster(){
        return clientThruster;
    }
    public void setHostThruster(float f){
        hostThruster = f;
    }
    public float getHostThruster(){
        return hostThruster;
    }

    public void setX(float f){
        newPosition =true;
        x=f;
    }
    public float getX(){
        newPosition = false;
        return x;
    }
    public void setY(float f){y=f;}
    public float getY(){return y;}
    public void setR(float f){r=f;}
    public float getR(){return r;}

    public boolean isNewPosition(){
        return newPosition;
    }

    AndroidNetworking(){
        clientThruster =0;
        hostThruster=0;
    }

    public String checkIP() {
        // The following code loops through the available network interfaces
        // Keep in mind, there can be multiple interfaces per device, for example
        // one per NIC, one per active wireless and the loopback
        // In this case we only care about IPv4 address ( x.x.x.x format )
        List<String> addresses = new ArrayList<String>();
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface ni : Collections.list(interfaces)) {
                for (InetAddress address : Collections.list(ni.getInetAddresses())) {
                    if (address instanceof Inet4Address) {
                        addresses.add(address.getHostAddress());
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        // Print the contents of our array to a string.  Yeah, should have used StringBuilder
        String ipAddress = new String("");
        for (String str : addresses) {
            if (!str.contains("127.0.0.1"))
                ipAddress = ipAddress + str + "\n";
        }
        Log.i("IPs", ipAddress);

        return ipAddress;
    }




    public void startListeningForThruster(HostMenu hm){

        new Thread(new ThrusterListener(this,hm)).start();
    }

    class ThrusterListener implements Runnable{

        Networking netw;
        DatagramSocket serverSocket;
        byte[] receiveData;
        DatagramPacket receivePacket;
        HostMenu hostMenu;

        ThrusterListener(Networking netw, HostMenu hm){
            this.netw = netw;
            this.hostMenu = hm;
            serverSocket = null;
            try {
                serverSocket = new DatagramSocket(62448);
            } catch (SocketException e) {
                e.printStackTrace();
            }
            receiveData = new byte[4];
            receivePacket = new DatagramPacket(receiveData, receiveData.length);
        }

        @Override
        public void run(){

            while(true) {
                try {
                    serverSocket.receive(receivePacket);
                    clientAddres = receivePacket.getAddress();
                    clientPort = receivePacket.getPort();
                    Log.i("Sender details", clientAddres.toString()+":"+Integer.toString(clientPort));
                    break;
                } catch (IOException e) {
                    Log.i("UDP", "Datagram socket didnt recieved");
                    e.printStackTrace();
                }
            }

            hostMenu.gotoGame();

            while(true) {
                try {
                    serverSocket.receive(receivePacket);
                        Log.i("Thruster status is", new String( receivePacket.getData()));
                        ByteBuffer bb = ByteBuffer.wrap(receivePacket.getData());

                        netw.setClientThruster(bb.getFloat());

                } catch (IOException e) {
                    Log.i("UDP", "Datagram socket didnt recieved");
                    e.printStackTrace();
                }
            }

        }

    }
    public void startListeningForPosition(){

        new Thread(new PositionListener(this)).start();
    }
    class PositionListener implements Runnable{

        Networking netw;
        DatagramSocket serverSocket;
        public byte[] receiveData;
        DatagramPacket receivePacket;

        PositionListener(Networking netw){
            this.netw = netw;
            serverSocket = null;
            try {
                serverSocket = new DatagramSocket(62448);
            } catch (SocketException e) {
                e.printStackTrace();
            }
            receiveData = new byte[4*5];
            receivePacket = new DatagramPacket(receiveData, receiveData.length);
        }

        @Override
        public void run(){
            while(true) {
                try {
                    Log.i("position is", "waiting");
                    serverSocket.receive(receivePacket);
                    Log.i("position is", new String( receivePacket.getData()));

                    ByteBuffer bb = ByteBuffer.wrap(receivePacket.getData());
                    netw.setX(bb.getFloat());
                    netw.setY(bb.getFloat());
                    netw.setR(bb.getFloat());
                    netw.setHostThruster(bb.getFloat());
                    netw.setGameState(bb.getFloat());

                } catch (IOException e) {
                    Log.i("UDP", "Datagram socket didnt recieved");
                    e.printStackTrace();
                }
            }

        }

    }

    public void startUDP(){
        try {
            senderSocket = new DatagramSocket(62447);
        } catch (SocketException e) {
            Log.i("UDP","Datagram socket cannot be created");
            e.printStackTrace();
        }
        Log.i("UDP","Datagram socket created");
    }



    public void sendThrusterStatus(float isOn,  InetAddress IPAddress ) {

        byte[] sendData = float2ByteArray(isOn);

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 62448);
        try {
            senderSocket.send(sendPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendPosition(float x, float y, float r, float isOn, float gameState) {


        byte[] sendData;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );

        try {
            outputStream.write(float2ByteArray(x));
            outputStream.write(float2ByteArray(y));
            outputStream.write(float2ByteArray(r));
            outputStream.write(float2ByteArray(isOn));
            outputStream.write(float2ByteArray(gameState));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sendData = outputStream.toByteArray();


        DatagramPacket sendPacket = null;//changed TODO

            Log.i("Client is ",clientAddres.toString()+":"+clientPort);
            sendPacket = new DatagramPacket(sendData, sendData.length, clientAddres, 62448);

        try {
            senderSocket.send(sendPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static byte [] float2ByteArray (float value)
    {
        return ByteBuffer.allocate(4).putFloat(value).array();
    }



//    public void startServer(){
//        // Now we create a thread that will listen for incoming socket connections
//        new Thread(new Runnable(){
//            @Override
//            public void run() {
//                ServerSocketHints serverSocketHint = new ServerSocketHints();
//                // 0 means no timeout.  Probably not the greatest idea in production!
//                serverSocketHint.acceptTimeout = 0;
//
//                // Create the socket server using TCP protocol and listening on 9021
//                // Only one app can listen to a port at a time, keep in mind many ports are reserved
//                // especially in the lower numbers ( like 21, 80, etc )
//                ServerSocket senderSocket = Gdx.net.newServerSocket(Net.Protocol.TCP, 9021, serverSocketHint);
//
//                // Loop forever
//                while(true){
//                    /* Create a socket */
//                    Socket socket = senderSocket.accept(null);
//
//                    // Read data from the socket into a BufferedReader
//                    BufferedReader buffer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//                    while(true){
//                        try {
//                            // Read to the next newline (\n) and display that text on labelMessage
//                            Log.i("INCOMING:", buffer.readLine());
//
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        }).start(); // And, start the thread running
//
//    }

}

