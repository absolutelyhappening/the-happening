package art.absolutelyhappening.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.World;
import com.codeandweb.physicseditor.PhysicsShapeCache;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import art.absolutelyhappening.happening.BodySprite;
import art.absolutelyhappening.happening.Ship;
import art.absolutelyhappening.happening.happening;
import art.absolutelyhappening.menu.FailScreen;
import art.absolutelyhappening.menu.WinScreen;

/**
 * Created by Dluuugi on 24/11/2017.
 */

public class ClientGameScreen implements Screen {
    private happening game;

    World world;
    Ship ship;
    ArrayList<BodySprite> bodies;
    PhysicsShapeCache physicsBodies;
    SpriteBatch batch;
    Texture img;
    Texture terrainTexture;
    Texture platformTexture;

    Texture background;
    Sprite backgroundSprite;

    InetAddress addr;
    public ClientGameScreen(happening game) {
        this.game = game;

        Gdx.app.log("info","client game");
        Box2D.init();
		bodies = new ArrayList<BodySprite>();
		batch = new SpriteBatch();
		img = new Texture("spaceship1.png");
        terrainTexture = new Texture("terrain.png");
        platformTexture = new Texture("platform.png");
		world = new World(new Vector2(0,-1f),false);
		physicsBodies = new PhysicsShapeCache("physics.xml");

        background = new Texture("background.jpg");
        backgroundSprite = new Sprite(background);
        backgroundSprite.setSize(30000,30000);
        backgroundSprite.setPosition(-5000,-5000);

        Gdx.app.log("info","client game2");

        bodies.add(new BodySprite(terrainTexture, new Vector2(0, 0),
                physicsBodies.createBody("terrain", world, 1/BodySprite.PX_TO_METER, 1/BodySprite.PX_TO_METER)));

        bodies.add(new BodySprite(platformTexture, new Vector2(25, 10),
                physicsBodies.createBody("platform", world, 1/BodySprite.PX_TO_METER, 1/BodySprite.PX_TO_METER)));

        ship = new Ship(img,new Vector2(15,10),
				physicsBodies.createBody("spaceship1",world,1/BodySprite.PX_TO_METER,1/BodySprite.PX_TO_METER));

        game.netw.startUDP();
        game.netw.startListeningForPosition();

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(null);
        Gdx.input.getTextInput(new Input.TextInputListener() {
            @Override
            public void input(String text) {
                try {
                    addr = InetAddress.getByName(text);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                    Gdx.app.exit();
                }
            }

            @Override
            public void canceled() {
                Gdx.app.exit();
            }
        },"server ip", "10.5.0.23", "server ip");
    }

    @Override
    public void render(float delta) {
        ship.update(0,0);
        //world.step(delta, 5, 5);
        batch.setProjectionMatrix(ship.getCamera().combined);

        if(game.netw.isNewPosition()){
            ship.setState(new Vector2(game.netw.getX(),game.netw.getY()), game.netw.getR());
        }
        Gdx.app.log("MAINE", ""+game.netw.getGameState());
        if(game.netw.getGameState() == 1f){
            game.setScreen(new WinScreen(this.game));
        }else if(game.netw.getGameState() == 2f){
            game.setScreen(new FailScreen(this.game));
        }

        if(addr != null){
            game.netw.sendThrusterStatus(Gdx.input.isTouched()? 1.0f :0.0f,  addr);
        }

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();

        backgroundSprite.draw(batch);
        for (BodySprite body : bodies) {
            body.getSprite().draw(batch);
        }
        ship.getSprite().draw(batch);
        batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
