package art.absolutelyhappening.happening;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

import art.absolutelyhappening.game.GameScreen;

/**
 * Created by Dluuugi on 25/11/2017.
 */

public class CollisionManager implements ContactListener {
    GameScreen game;
    long startTime;
    boolean areCollided;
    boolean hasCrashed = false;

    public static final float CRASH_VELOCITY = 5;

    public CollisionManager(GameScreen game) {
        this.game = game;
        startTime = 0;
        areCollided = false;
    }

    public boolean hasLanded() {
        if (areCollided && (System.currentTimeMillis() - startTime > 1000)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean hasCrashed(){
        return hasCrashed;
    }

    @Override
    public void beginContact(Contact contact) {
        startTime = System.currentTimeMillis();
        areCollided = true;

        if(contact.getFixtureA().getBody().getUserData()!=null){
            if(contact.getFixtureA().getBody().getLinearVelocity().len()>=CRASH_VELOCITY){
                hasCrashed = true;
            }
        }
        if(contact.getFixtureB().getBody().getUserData()!=null){
            if(contact.getFixtureB().getBody().getLinearVelocity().len()>=CRASH_VELOCITY){
                hasCrashed = true;
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
        startTime = 0;
        areCollided = false;
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
